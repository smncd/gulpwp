# GulpWP
## Gulp for WordPress

**GulpWP** is a drop-in [gulp.js](https://gulpjs.com/) configuration file for WordPress theme development.

The idea behind it is to simplify the folder structure, separating compiled and source assets (scripts, stylesheets, etc).

## Folder structure

Gulp looks for files in these directories.

```
.
├── src/
│   ├── blocks
│   │   └── *
│   │       └── block.jsx
│   │       └── editor.scss 
│   │       └── style.scss
│   ├── images
│   │   └── *
│   ├── scripts
│   │   └── *.js
│   ├── styles
│   │   └── *.scss
│   └── typefaces
│       └── *
├── *.php
├── LICENSE
├── screenshot.png
└── style.css
```
## Compiled folder structure

```
.
└── assets
│   ├── css
│   │   └── blocks
│   │   │   ├── editor.min.css
│   │   │   └── style.min.css
│   │   └── *.min.css
│   ├── images
│   │   └── *
│   ├── js
│   │   └── blocks.min.js
│   │   └── *.js
│   └── typefaces
│       └── *
├── *.php
├── LICENSE
├── screenshot.png
└── style.css
```
Gulp creates the assets/ folder on run, remember to reference that folder in your php, and never the src/ folder.


## Setup

### Install

Clone or download *gulpfile.js* from this repo

### .gitignore

Some files should not be commited to your git repo. Add the following to your *.gitignore*:
```shell
# Dependencies
node_modules/

# Config
.env

# Development
/assets/

# Production
/dist/
```

### Dependencies

In order to use GulpWP you need [Node.js and NPM](https://nodejs.org/en/) installed on your machine.   
Initialise NPM by executing `npm init` in the root of your project.  
After you have done that, execute the following command to install the dependencies required for GulpWP:

```shell
npm install @babel/core @babel/preset-env @babel/preset-react babelify browserify cross-env dotenv gulp gulp-autoprefixer gulp-babel gulp-buffer gulp-clean gulp-clean-css gulp-concat gulp-imagemin gulp-newer gulp-rename gulp-sass gulp-tap gulp-uglify gulp-util gulp-webp gulp-zip --save-dev
```

### .env

GulpWP uses a *.env* file for environment variables. Add the following to your *.env*:

```shell
THEME_NAME="your-theme-name"
```

### Browserlist

To set the autoprefixer environment variables, add `"browserlist"`, like shown below, to *package.json*.

```json
"browserslist": [
    "> 0.2%",
    "last 4 versions",
    "dead"
  ]
```

### Scripts

In order to execute the gulp tasks, update `"scripts"` in *package.json* to the following:

```json
"scripts": {
    "dev": "gulp",
    "build": "gulp build",
    "bundle": "cross-env NODE_ENV=production gulp bundle"
  }
```

## Usage

To start theme development and watch for file changes, run:

```shell
npm run dev
```

To start theme development without watching, run:
```shell
npm run build
```

To compile the theme, run:

```shell
npm run bundle
```

## Changelog

### v1.3
* Using `browserify` when bundling scripts, allowing for the use of ES modules.
* Ignoring scripts that start with `_`, this is to allow splitting into modules without having each module be bundled.
* Blocks are no longer put into subfolders when bundled.
* Minor restructuring

## License

GulpWP is licensed under the [GNU General Public License v2](./LICENSE)
