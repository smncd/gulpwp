/**!
 * GulpWP v1.4.0
 * Copyright (C) 2021 Simon Lagerlöf Petéus
 * URI: https://gitlab.com/twentyonepenguins/gulpwp
 * License: GNU General Public License v2
 */
 'use strict';

/**
 * Import dependencies
 */
require('dotenv').config();
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const babelify = require('babelify');
const browserify = require('browserify');
const buffer = require('gulp-buffer');
const clean = require('gulp-clean');
const cleancss = require('gulp-clean-css');
const concat = require('gulp-concat');
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const tap = require('gulp-tap');
const uglify = require('gulp-uglify');
const util = require('gulp-util');
const webp = require('gulp-webp');
const zip = require('gulp-zip');

/**
 * Theme config
 */
const production = process.env.NODE_ENV;
const themeName = process.env.THEME_NAME;

/**
 * Paths and files
 */
const dir = {
  src: 'src/',
  assets: 'assets/',
  dist: 'dist/',
  theme: 'dist/' + themeName + '/',
};

/**
 * Task for styles.
 */
const stylesProps = {
  src: dir.src + 'styles/**/*.scss',
  dest: production 
    ? dir.theme + dir.assets + 'css' 
    : dir.assets + 'css',
};
const styles = () => {
  const { src, dest } = stylesProps;

  const minify = production ? cleancss() : util.noop();
  return gulp
    .src(src)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(
      rename({
        suffix: '.min',
      })
    )
    .pipe(minify)
    .pipe(gulp.dest(dest));
};

/**
 * Task for JavaScript
 */
const scriptsProps = {
  src: [
    dir.src + 'scripts/**/*.js', 
    '!' + dir.src + 'scripts/**/_*.js'
  ],
  dest: production ? dir.theme + dir.assets + 'js' : dir.assets + 'js',
};
const scripts = () => {
  const { src, dest } = scriptsProps;

  const minify = production ? uglify() : util.noop();

  return gulp
    .src(src)
    .pipe(
      tap((file) => {
        file.contents = browserify(file.path)
        .transform(babelify.configure({
          presets: ["@babel/env"]
        }))
        .bundle()
      })
    )
    .pipe(buffer())
    .pipe(minify)
    .pipe(rename({
      extname: '.min.js'
    }))
    .pipe(gulp.dest(dest));
};

/**
 * Image task
 */
const imagesProps = {
  src: dir.src + 'images/**/*',
  dest: production 
    ? dir.theme + dir.assets + 'images'
    : dir.assets + 'images',
};
const images = () => {
  const { src, dest } = imagesProps;

  const webpConvert = production ? webp() : util.noop();
  return (
    gulp
      .src(src)
      .pipe(newer(dest))
      .pipe(imagemin())
      .pipe(gulp.dest(dest))
      .pipe(webpConvert)
      .pipe(gulp.dest(dest))
  );
}

/**
 * Task for typefaces.
 *
 * Files are compiled in `assets/typefaces/`.
 */
const typefacesProps = {
  src: dir.src + 'typefaces/**/*',
  dest: production
    ? dir.theme + dir.assets + 'typefaces'
    : dir.assets + 'typefaces',
};
const typefaces = () => {
  const { src, dest } = typefacesProps;

  return gulp
    .src(src)
    .pipe(gulp
    .dest(dest));
}

/**
 * Task for Blocks
 */
const blocksProps = {
  style: {
    frontEndSrc: dir.src + 'blocks/**/style.scss',
    editorSrc: dir.src + 'blocks/**/editor.scss',
    dest: production
      ? dir.theme + dir.assets + 'css/blocks'
      : dir.assets + 'css/blocks',
  },
  script: {
    src: dir.src + 'blocks/**/block.{jsx,js}',
    dest: production 
      ? dir.theme + dir.assets + 'js/blocks'
      : dir.assets + 'js/blocks',
  },
  watch: dir.src + 'blocks/**/*',
};
const blocks = () => {
  const { 
    style: styleProps,
    script: scriptProps
  } = blocksProps;

  const style = (src, exportName) => {
    const minify = production ? cleancss() : util.noop();

    return gulp
      .src(src)
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({ cascade: false }))
      .pipe(concat(exportName))
      .pipe(minify)
      .pipe(gulp.dest(styleProps.dest));
  };

  const scripts = (src) => {
    const minify = production ? uglify() : util.noop();

    return gulp
      .src(src)
      .pipe(
        babel({
          presets: [
            [
              '@babel/preset-react',
              {
                pragma: 'wp.element.createElement',
              },
            ],
            '@babel/preset-env',
          ]
        })
      )
      .pipe(minify)
      .pipe(
        rename((path) => {
          const blockName = path.dirname.match(/([^\/]*)\/*$/)[1]
          
          path.dirname = '';
          path.basename = blockName;
          path.extname = '.min.js';
        })
      )
      .pipe(gulp.dest(scriptProps.dest));
  };
  return (
    style(styleProps.frontEndSrc, 'style.min.css'),
    style(styleProps.editorSrc, 'editor.min.css'),
    scripts(scriptProps.src)
  );
};

/**
 * Task for PHP.
 */
const phpProps = {
  src: '**/*.php',
  dest: dir.theme,
};
const php = () => {
  const { src, dest } = phpProps;

  return gulp
    .src(src)
    .pipe(gulp.dest(dest));
}

/**
 * Misc. items
 */
const miscProps = {
  src: ['LICENSE', 'screenshot.png', 'style.css'],
  dest: dir.theme,
};
const misc = () => {
  const { src, dest } = miscProps;

  return gulp
    .src(src, {"allowEmpty": true})
    .pipe(gulp.dest(dest))
}

/**
 * Clean dist folder
 */
const cleanBuildFolder = () =>
  gulp.src(dir.dist + '*').pipe(clean({ force: true }));

/**
 * Zip up the theme
 */
const pack = () =>
  gulp
    .src(dir.theme + '**/*')
    .pipe(zip(themeName + '.zip'))
    .pipe(gulp.dest(dir.dist));

/**
 * Run all tasks
 */
gulp.task('build', gulp.series(styles, scripts, images, typefaces, blocks));

/**
 * Watch for file changes
 */
gulp.task('watch', () => {
  gulp.watch(stylesProps.src, styles);

  gulp.watch(scriptsProps.src, scripts);

  gulp.watch(imagesProps.src, images);

  gulp.watch(typefacesProps.src, typefaces);

  gulp.watch(blocksProps.watch, blocks);
});

/**
 * Default task
 */
gulp.task('default', gulp.series('build', 'watch'));

/**
 * Bundle theme
 */
gulp.task(
  'bundle',
  gulp.series(
    cleanBuildFolder,
    gulp.series(php, misc, scripts, styles, images, typefaces, blocks),
    pack
  )
);
